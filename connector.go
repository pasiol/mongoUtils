package mongoUtils

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConfig struct {
	Db       string
	User     string
	Password string
	URI      string
}

func generateMongoURI(m MongoConfig, connectDirect bool) (string, error) {
	if m.URI == "" || m.Db == "" {
		return "", errors.New("malformed mongo config struct")
	}
	var credentials, parameters string
	if m.User != "" && m.Password != "" {
		credentials = fmt.Sprintf("%s:%s@", m.User, m.Password)
	}
	if connectDirect {
		parameters = "?connect=direct"
	} else {
		parameters = "?retryWrites=true&w=majority"
	}
	if strings.Contains(m.URI, "localhost") || strings.Contains(m.URI, "127.0.0.1") {
		return fmt.Sprintf("mongodb://%s%s/%s%s", credentials, m.URI, m.Db, parameters), nil
	} else {
		return fmt.Sprintf("mongodb+srv://%s%s/%s%s", credentials, m.URI, m.Db, parameters), nil
	}
}

func ConnectOrDie(m MongoConfig, connectDirect bool) (a *mongo.Database, b *mongo.Client) {
	MongoURI, err := generateMongoURI(m, connectDirect)
	if err != nil {
		log.Fatalf("fatal: %s", err)
	}
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoURI))
	if err != nil {
		log.Fatalf("fatal: %s", err)
	}
	err = client.Connect(context.TODO())
	if err != nil {
		log.Fatalf("fatal: %s", err)
	}
	var DB = client.Database(m.Db)
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatalf("fatal: %s", err)
	}

	return DB, client
}

func ConnectOrFail(m MongoConfig, connectDirect bool) (a *mongo.Database, b *mongo.Client, err error) {
	mongoURI, err := generateMongoURI(m, connectDirect)
	if err != nil {
		return &mongo.Database{}, &mongo.Client{}, err
	}
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	if err != nil {
		return &mongo.Database{}, &mongo.Client{}, err
	}
	err = client.Connect(context.TODO())
	var DB = client.Database(m.Db)
	if err != nil {
		return &mongo.Database{}, &mongo.Client{}, err
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return &mongo.Database{}, &mongo.Client{}, err
	}
	return DB, client, nil
}
