package mongoUtils

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoWriter struct {
	DB *mongo.Database
}

func (writer *MongoWriter) Write(p []byte) (n int, err error) {
	collection := writer.DB.Collection("log")
	_, err = collection.InsertOne(context.TODO(), bson.D{{"timeStamp", time.Now()}, {"message", string(p)}})
	if err != nil {
		return 0, err
	}
	return len(p), nil
}
