package mongoUtils

import (
	"strings"
	"testing"

	"gitlab.com/pasiol/dockerUtils4Tests"
)

func TestConnecOrFailLocalhost(t *testing.T) {
	mongo := MongoConfig{
		Db:       "test",
		User:     "",
		Password: "",
		URI:      "localhost:27017",
	}
	_, _ = dockerUtils4Tests.StopContainer("mongoutilstest")
	if err := dockerUtils4Tests.StartContainer("mongo:latest", "mongoutilstest", 27017, true); err != nil {
		t.Logf("error should be nil: %s", err)
		t.Fail()
	}

	db, client, err := ConnectOrFail(mongo, false)
	if err != nil {
		t.Logf("error should be nil: %s", err)
		t.Fail()
	}
	t.Logf("Database: %v\n Client: %v", db, client)

}

func TestConnecOrFailLocalhostNoMongo(t *testing.T) {
	mongo := MongoConfig{
		Db:       "test",
		User:     "",
		Password: "",
		URI:      "localhost:27017",
	}
	_, err := dockerUtils4Tests.StopContainer("mongoutilstest")
	if err != nil && err.Error() != "Error response from daemon: page not found" {
		t.Logf("error should be nil: %s", err)
		t.Fail()
	}

	db, client, err := ConnectOrFail(mongo, false)
	if err != nil && !strings.Contains(err.Error(), "server selection error: server selection timeout") {
		t.Logf("error should be nil: %s", err)
		t.Fail()
	}
	t.Logf("Database: %v\n Client: %v", db, client)

}
